const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./src/index.ts",
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.mp3$/i,
        type: "asset/resource",
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
    assetModuleFilename: "audio/[hash][ext][query]",
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "AOC 2022",
      template: "src/day1/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "AOC 2022 Day 2",
      filename: "day2.html",
      template: "src/day2/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "AOC 2022 Day 3",
      filename: "day3.html",
      template: "src/day3/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "AOC 2022 Day 3",
      filename: "day4.html",
      template: "src/day4/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "AOC 2022 Day 5",
      filename: "day5.html",
      template: "src/day5/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "AOC 2022 Day 6",
      filename: "day6.html",
      template: "src/day6/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "AOC 2022 Day 8",
      filename: "day8.html",
      template: "src/day8/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "AOC 2022 Day 9",
      filename: "day9.html",
      template: "src/day9/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "AOC 2022 Day 10",
      filename: "day10.html",
      template: "src/day10/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "AOC 2022 Day 11",
      filename: "day11.html",
      template: "src/day11/index.html",
    }),
    new HtmlWebpackPlugin({
      title: "Conway Game Of Life",
      filename: "gol.html",
      template: "src/ch5-game-of-life/gol.html",
    }),
    new HtmlWebpackPlugin({
      title: "Conway Game Of Life Multithreaded",
      filename: "golM.html",
      template: "src/ch5-game-of-life/thread-gol.html",
    }),
  ],
  devServer: {
    headers: {
      'Cross-Origin-Embedder-Policy': 'require-corp',
      'Cross-Origin-Opener-Policy': 'same-origin',
    },
    static: [
      path.join(__dirname, "dist"),
      path.join(__dirname, "./src/day3/audio"),
      path.join(__dirname, "./src/day9/images"),
      path.join(__dirname, "./src/day11/images"),
    ],
    compress: true,
    port: 3000,
  },
};
