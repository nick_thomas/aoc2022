interface Element {
  getAttribute(qualifiedName: string): string;
}
