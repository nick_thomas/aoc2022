import { subtract, add } from "ramda";
type IContent = {
  image: string;
  caption: string;
};
const content: Array<IContent> = [
  {
    image: "dave-hoefler-okUIdo6NxGo-unsplash.jpg",
    caption: "Photo by Dave Hoefler on Unsplash",
  },
  {
    image: "sherman-yang-VBBGigIuaDY-unsplash.jpg",
    caption: "Photo by Sherman Yang n Unsplash",
  },
  {
    image: "jakob-owens-EwRM05V0VSI-unsplash.jpg",
    caption: "Photo by Jakob Owens on Unsplash",
  },
  {
    image: "finding-dan-dan-grinwis-O35rT6OytRo-unsplash.jpg",
    caption: "Photo by Dan Grinwis on Unsplash",
  },
  {
    image: "vincentiu-solomon-ln5drpv_ImI-unsplash.jpg",
    caption: "Photo by Vincentiu Solomon on Unsplash",
  },
  {
    image: "silas-baisch-Wn4ulyzVoD4-unsplash.jpg",
    caption: "Photo by Silas Baisch on Unsplash",
  },
  {
    image: "eugene-golovesov-EXdXp7Z4X4w-unsplash.jpg",
    caption: "Photo by Eugene Golovesov on Unsplash",
  },
  {
    image: "jennifer-reynolds-_8HI5xf4TkE-unsplash.jpg",
    caption: "Photo by Jennifer reynolds on Unsplash",
  },
  {
    image: "kellen-riggin-SIBOiXKg0Ds-unsplash.jpg",
    caption: "Photo by Kellen Riggin on Unsplash",
  },
  {
    image: "rafael-hoyos-weht-zhkAp8DGkxw-unsplash.jpg",
    caption: "Photo by Rafael Hoyos on Unsplash",
  },
  {
    image: "sonya-romanovska-wzdXhyi3AiM-unsplash.jpg",
    caption: "Photo by Sonya Romanovska on Unsplash",
  },
];

function returnFirstValue<T>(item: Array<T>): T {
  const [first, ...rest] = [...item];
  return first;
}

function returnFirstElementNode(item: HTMLCollectionOf<Element>): Element {
  const [first, ...rest] = [...item];
  return first;
}

function generateCarouselImages(content: Array<IContent>): DocumentFragment {
  const node = document.createDocumentFragment();
  content.forEach((content) => {
    const list = <HTMLLIElement>document.createElement("li");
    const link = document.createElement("a");
    const image = document.createElement("img");
    image.setAttribute("src", `./${content.image}`);
    link.append(image);
    list.append(link);
    node.appendChild(list);
  });
  return node;
}

function generateFeature(
  contents: Array<IContent>,
  index: number
): DocumentFragment {
  if (index < 0) {
    index = contents.length - 1;
  }
  if (index > contents.length) {
    index = 0;
  }
  const content = contents[index];
  const node = document.createDocumentFragment();
  const caption = document.createElement("div");
  const image = document.createElement("img");
  const contentDiv = document.createTextNode(content.caption);
  image.setAttribute("src", `./${content.image}`);
  caption.setAttribute("class", "caption");
  caption.appendChild(contentDiv);
  node.appendChild(image);
  node.appendChild(caption);
  return node;
}

const setupNavigation = () => {
  const [left, right] = [
    document.getElementsByClassName("left"),
    document.getElementsByClassName("right"),
  ];
  return [left, right].map((item) => returnFirstElementNode(item));
};

const generateLandingCarousel = () => {
  const thumbnails = returnFirstElementNode(
    document.getElementsByClassName("thumbnails")
  );
  const unorderedList = returnFirstElementNode(thumbnails.children);
  const carousel = generateCarouselImages(content);

  unorderedList.append(carousel);
};

const dayNineMain = () => {
  generateLandingCarousel();
  const [left, right] = setupNavigation();
  const feature = returnFirstElementNode(
    document.getElementsByClassName("feature")
  );
  // @TODO fix left iterate
  let start = 0;
  // Iterate through the active elements by updating the feature
  left.addEventListener("click", (e) => {
    start = subtract(start, 1);
    let newFeature = generateFeature(content, start);
    feature.replaceChildren(newFeature);
  });
  right.addEventListener("click", (e) => {
    start = add(start, 1);
    let newFeature = generateFeature(content, start);
    feature.replaceChildren(newFeature);
  });
};

export default dayNineMain;
