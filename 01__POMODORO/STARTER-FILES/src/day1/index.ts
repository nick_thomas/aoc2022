const startTimer = () => {
  const timeValue = document.getElementsByTagName("input");
  let [minuteTime, secondTime]: number[] = Array.from(timeValue).map((item) =>
    parseInt(item.value)
  );
  const display = document.getElementById("time") as HTMLElement;
  let totalTime = minuteTime * 60000 + secondTime * 1000;
  if (totalTime === 0) {
    return;
  }
  const countDown = setInterval(() => {
    console.log("printing stuffs");
    if (totalTime === 0) {
      clearInterval(countDown);
    }
    totalTime -= 1000;
    let minuteDisplay, secondDisplay;
    minuteDisplay = (totalTime / 60000).toFixed();
    secondDisplay = ((totalTime % 60000) / 1000).toFixed(0);
    display.innerHTML = `${minuteDisplay}: ${secondDisplay}`;
  }, 1000);
};

const main = () => {
  const [startBtn, ...rest] = document.getElementsByClassName("start");
  startBtn.addEventListener("click", startTimer);
};

export default main;
