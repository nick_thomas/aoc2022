// Move the knob on the range and the dollar amount above it will update.
const daySixMain = () => {
  const priceRange = document.getElementById("priceRange") as HTMLInputElement;
  priceRange.addEventListener("click", (event: MouseEvent) => {
    const [dollars, ..._rest] = document.getElementsByClassName("dollars");
    let target = event.target;
    if (target === null) return;
    let value = (event.target as HTMLInputElement).valueAsNumber;

    let node = document.createElement("span");
    node.classList.add("dollars");
    node.innerHTML = `${value.toString().substring(0)}`;
    dollars.replaceWith(node);

    console.log(value);
  });
};

export default daySixMain;
