////Users should be able to:
//
//    See the list of podcast episodes
//    Checking off an episode will mark the episode as watched and will change the opacity of the episode title and strike-through it (not illustrated above)
//    The user will be able to select one episode, then shift click to check off another episode and it will mark off all the episodes in-between.
type IEpisode = {
  id: number;
  name: string;
};
type IEpisodes = Array<IEpisode>;

const episodes = [
  {
    id: 1,
    name: "James Q Quick's Origin Story",
  },
  {
    id: 2,
    name: "Amy Dutton's Origin Story",
  },
  {
    id: 3,
    name: "The Tech Behind Compressed.fm",
  },
  {
    id: 4,
    name: "Starting a New Development Project",
  },
  {
    id: 5,
    name: "How Do you Start a New Design Project?",
  },
  {
    id: 6,
    name: "Freelancing (Part 1)",
  },
  {
    id: 7,
    name: "Freelancing (Part 2)",
  },
  {
    id: 8,
    name: "The Tech Behind jamesqquick.com",
  },
  {
    id: 9,
    name: "Teh Tech Behind SelfTeach.me",
  },
  {
    id: 10,
    name: "Design Fundamentals (Part 1)",
  },
  {
    id: 11,
    name: "Design Fundamentals (Part 2)",
  },
  {
    id: 12,
    name: "Productivity: Tools, Tips, and Workflows",
  },
  {
    id: 13,
    name: "The Future of Code with No Code",
  },
  {
    id: 14,
    name: "Building the Perfect Desk Setup",
  },
  {
    id: 15,
    name: "Everything You Need to Know to Get Started in SvelteKit",
  },
  {
    id: 16,
    name: "Live Streaming for Beginners",
  },
  {
    id: 17,
    name: "All Things Automated",
  },
  {
    id: 18,
    name: "Amy Gives James a Design Consult",
  },
  {
    id: 19,
    name: "Figma for Everyone",
  },
  {
    id: 20,
    name: "Learning and Building in Public",
  },
  {
    id: 21,
    name: "Getting Your First Dev Job",
  },
  {
    id: 22,
    name: "Hiring a Designer or Getting Your First UI / UX Job",
  },
  {
    id: 23,
    name: "IRL Freelance Proposal",
  },
  {
    id: 24,
    name: "Getting Started on YouTube",
  },
  {
    id: 25,
    name: "Starting your own Podcast",
  },
  {
    id: 26,
    name: "How Blogging Can Change Your Career",
  },
  {
    id: 27,
    name: "Talking to Some of Our Favorite Content Creators",
  },
  {
    id: 28,
    name: "Our Favorite Things: A Crossover Episode with Web Dev Weekly",
  },
  {
    id: 29,
    name: "Freelancing IRL: Unveiling a Site Redesign",
  },
  {
    id: 30,
    name: "Wordpress in 2021",
  },
  {
    id: 31,
    name: "Struggle Bus",
  },
  {
    id: 32,
    name: "Getting Started with TypeScript",
  },
  {
    id: 33,
    name: "Small Design Tweaks that Make All the Difference",
  },
  {
    id: 34,
    name: "Getting git",
  },
  {
    id: 35,
    name: "Crossover Episode with Purrfect Dev",
  },
  {
    id: 36,
    name: "SVGs FTW",
  },
  {
    id: 37,
    name: "Building a Course",
  },
];

const generateList = () => {
  let updatedList = episodes.map(
    ({ id, name }) => `<li>

          <label for="episode-${id}">
            <input type="checkbox" name="episode-${id}" id="episode-${id}" />
            <span>${id} || ${name}</span>
          </label>
                                   </li>`
  );
  updatedList = ["<ul>", ...updatedList, "</ul>"];
  return updatedList.join("");
};

const generatePodcasts = () => {
  const [getDiv, ..._rest] =
    document.getElementsByClassName("podcastEpisodeBody");
  const getGeneratedList = generateList();
  getDiv.insertAdjacentHTML("afterbegin", `${getGeneratedList}`);
};

const getIdRange =
  (range: [number, number], stack: Array<Event>) => (item: any) => {
    // put range and stack outside the scope of current function
    stack.push(item);
    if (stack.length >= 3) {
      let [_drop, first, second] = [...stack];
      // How to get the end value?
      if ((<HTMLElement>first.target).matches("input")) {
        let [id, ..._] = (<HTMLElement>first.target).id.match(/\d+/) ?? [0];
        let [nextId, ...__] = (<HTMLElement>second.target).innerHTML.match(
          /\d+/
        ) ?? [0];
        range[0] = parseInt(id.toString());
        range[1] = parseInt(nextId.toString());
      } else {
        range = [0, 0];
      }
      stack.splice(0, stack.length);
    } else {
      range = [0, 0];
    }
    enableCheckBox(range);
  };

const enableCheckBox = (range: [number, number]) => {
  if (range.includes(0)) {
    return;
  }
  let start = range[0],
    end = range[1];
  if (range[0] > range[1]) {
    start = range[1];
    end = range[0];
  }
  for (let index = start; index <= end; index++) {
    let item = <HTMLInputElement>document.getElementById(`episode-${index}`);
    if (item !== null) {
      item.checked = true;
    }
  }
};

const dayFiveMain = () => {
  generatePodcasts();
  document.querySelectorAll(".podcastEpisodeBody").forEach((checkBox) => {
    let range: [number, number] = [0, 0];
    let stack: Array<Event> = [];
    const rangeResponse = getIdRange(range, stack);
    checkBox.addEventListener("click", (event) => {
      rangeResponse(event);
    });
  });
  // supply function for querySelectorAll
};

export default dayFiveMain;
