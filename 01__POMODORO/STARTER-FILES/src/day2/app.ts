type IMenu = {
  id: number;
  name: string;
  price: number;
  image: string;
  alt: string;
  count: number;
};
const menuItems: Array<IMenu> = [
  {
    id: 1,
    name: "French Fries with Ketchup",
    price: 223,
    image: "plate__french-fries.png",
    alt: "French Fries",
    count: 0,
  },
  {
    id: 2,
    name: "Salmon and Vegetables",
    price: 512,
    image: "plate__salmon-vegetables.png",
    alt: "Salmon and Vegetables",
    count: 0,
  },
  {
    id: 3,
    name: "Spaghetti Meat Sauce",
    price: 782,
    image: "plate__spaghetti-meat-sauce.png",
    alt: "Spaghetti with Meat Sauce",
    count: 0,
  },
  {
    id: 4,
    name: "Bacon, Eggs, and Toast",
    price: 599,
    image: "plate__bacon-eggs.png",
    alt: "Bacon, Eggs, and Toast",
    count: 0,
  },
  {
    id: 5,
    name: "Chicken Salad with Parmesan",
    price: 698,
    image: "plate__chicken-salad.png",
    alt: "Chicken Salad with Parmesan",
    count: 0,
  },
  {
    id: 6,
    name: "Fish Sticks and Fries",
    price: 634,
    image: "plate__fish-sticks-fries.png",
    alt: "Fish Sticks and Fries",
    count: 0,
  },
];

const generateMenu = () => {
  const getPanel = document.getElementById("menuPanel");
  if (getPanel === null) {
    throw new Error("Couldn't generate menu");
  }
  menuItems.forEach((menuItem) => {
    let { price } = menuItem;
    let displayPrice = price / 100;
    getPanel.insertAdjacentHTML(
      "afterend",
      `
      <li>
      <div class="plate">
      <img src="images/${menuItem.image}" alt="${menuItem.alt}" class="plate" />
      </div>
      <div class="content">
      <p class="menu-item">${menuItem.name}</p>
      <p class="price">$${displayPrice}</p>
      <button class="add" data="${menuItem.id}">Add to Cart</button>
      </div>
      </li>`
    );
  });
};

const updateCart = (carts: Array<IMenu>): Array<String> => {
  let computedCart = carts.map((cart) => {
    let price = cart.price / 100;
    return ` <li>
    <div class="plate">
    <img src="images/${cart.image}" alt="${cart.alt}" class="plate" />
    <div class="quantity">${cart.count}</div>
    </div>
    <div class="content">
    <p class="menu-item">${cart.name}</p>
    <p class="price">$${price}</p>
    </div>
    <div class="quantity__wrapper">
    <button class="decrease">
    <img src="images/chevron.svg" />
    </button>
    <div class="quantity">1</div>
    <button class="increase">
    <img src="images/chevron.svg" />
    </button>
    </div>
    <div class="subtotal">
    $${price}
    </div>
    </li>`;
  });
  return computedCart;
};

const generateCart = (carts: Array<IMenu>) => {
  let computedCart = updateCart(carts);
  let total = carts.reduce((acc, item) => (acc += item.price), 0) / 100;
  let computedOutput = `

  <h1>Your Cart</h1>
  <ul class="cart-summary">
  ${computedCart}
  </ul>
  <div class="totals">
  <div class="line-item">
  <div class="label">Subtotal:</div>
  <div class="amount price subtotal">$10.80</div>
  </div>
  <div class="line-item">
  <div class="label">Tax:</div>
  <div class="amount price tax">$1.05</div>
  </div>
  <div class="line-item total">
  <div class="label">Total:</div>
  <div class="amount price total">$${total}</div>
  </div>
  `;

  let ifCartExists = document.querySelector(".cart-summary") as HTMLElement;
  if (ifCartExists) {
    let parentNode = ifCartExists.parentNode;
    parentNode?.replaceChildren();
  }
  let populateCartDiv = document.querySelector(".panel.cart") as HTMLElement;
  populateCartDiv.insertAdjacentHTML("afterbegin", computedOutput);
};

const globalMenuItems: Array<IMenu> = [];

const addCart = () => {
  let addToCartBtn = document.querySelectorAll(".add");
  Array.from(addToCartBtn).forEach((button) => {
    button.addEventListener("click", () => {
      const updateId = button.getAttribute("data");

      if (checkIfNull(updateId)) {
        throw new Error("unexpected");
      } else {
        const idToCheck = parseInt(updateId);
        let [valueToAdd, ..._rest] = menuItems.filter(
          (item) => item.id === idToCheck
        );
        let addToMenu = globalMenuItems.find(
          (item) => item.id === valueToAdd.id
        );
        if (addToMenu) {
          addToMenu.count += 1;
        } else {
          valueToAdd.count += 1;
          globalMenuItems.push(valueToAdd);
        }
        generateCart(globalMenuItems);
      }
    });
  });
};

const checkIfNull = <T>(item: T | null): boolean => item === null;

const dayTwoMain = () => {
  generateMenu();
  addCart();
};

export default dayTwoMain;
