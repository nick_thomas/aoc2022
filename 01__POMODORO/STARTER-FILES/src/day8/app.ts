const daysOfWeekMap: { [index: string]: any } = {
  0: "SUN",
  1: "MON",
  2: "TUES",
  3: "WED",
  4: "THUR",
  5: "FRI",
  6: "SAT",
  /**
   * getDayOfWeek
   */
  get(day: string): string {
    return this[day];
  },
};

const WEATHER_ENDPOINT =
  "https://api.open-meteo.com/v1/forecast?latitude=45.424721&longitude=-75.695&daily=temperature_2m_max,temperature_2m_min,rain_sum,precipitation_probability_max&timezone=America%2FNew_York";

export interface IWeather {
  latitude: number;
  longitude: number;
  generationtime_ms: number;
  utc_offset_seconds: number;
  timezone: string;
  timezone_abbreviation: string;
  elevation: number;
  daily_units: DailyUnits;
  daily: Daily;
}

export interface DailyUnits {
  time: string;
  temperature_2m_max: string;
  temperature_2m_min: string;
  rain_sum: string;
  precipitation_probability_max: string;
}

export interface Daily {
  time: string;
  temperature_2m_max: number[];
  temperature_2m_min: number[];
  rain_sum: number[];
  precipitation_probability_max: number[];
}

interface ExtendedDailyWeather extends Daily {
  weather: string;
}

// look up weather for each map
const weatherComponent = (daily: Array<ExtendedDailyWeather>) => {
  return daily
    .map((day: ExtendedDailyWeather) => {
      const weatherConfig = iconNameToSizeMap.get(day.weather);
      const date = new Date(day.time);
      return `
      <div class ="day">
        <div class="day-of-week">${daysOfWeekMap.get(date.getUTCDay())}</div>
        <div class="date">${date.getDate()}</div>
        <div class="bar ${day.weather}">
          <div class="weather">
            <svg role="img" width="${weatherConfig.width}" height="${
        weatherConfig.height
      }" viewBox="0 0 ${weatherConfig.width} ${weatherConfig.height}">
              <use xlink:href="#${day.weather}"></use>
            </svg>
          </div>
          <div class="temperature">${
            day.temperature_2m_max
          }<span class="degrees">&deg;</span></div>
          <div class="content">
            <div class="precipitation">
              <svg role="img" class="icon">
                <use xlink:href="#precipitation"></use>
              </svg>
              ${day.precipitation_probability_max}%
            </div>
            <div class="low">
              <svg role="img" class="icon">
                <use xlink:href="#low"></use>
              </svg>
              ${day.temperature_2m_min}&deg;
            </div>
          </div>
        </div>
        </div>
`;
    })
    .join("");
};

interface IiconNameToSizeMap {
  width: number;
  height: number;
}

const iconNameToSizeMap = {
  cloudy: { width: 264, height: 166 },
  sunny: { width: 208, height: 213 },
  stormy: { width: 246, height: 187 },
  snowy: { width: 230, height: 196 },
  "partly-cloudy": { width: 230, height: 209 },
  rainy: { width: 160, height: 222 },
  get(weather: string): IiconNameToSizeMap {
    // complete the switch case
    switch (weather) {
      case "cloudy":
        return this.cloudy;
      case "stormy":
        return this.stormy;
      case "snowy":
        return this.snowy;
      case "partly-cloudy":
        return this["partly-cloudy"];
      case "rainy":
        return this.rainy;
      default:
        return this.sunny;
    }
  },
};

const getWeather = async () => {
  try {
    // check how to use awaited
    const response: Response = await fetch(WEATHER_ENDPOINT);
    if (response.ok) {
      const data: IWeather = await response.json();
      const parsedData = parseEndPointData(data.daily);
      return parsedData;
    } else {
      throw new Error("Response request failed");
    }
  } catch (e: unknown) {
    if (e instanceof Error) {
      throw e.message;
    } else {
      console.log(e);
      throw new Error("Something unexpected happened");
    }
  }
};
const parseEndPointData = (data: Daily): Array<ExtendedDailyWeather> => {
  const parsed: Array<ExtendedDailyWeather> = Object.entries(data).reduce(
    (prev, curr) => {
      const [key, value] = curr;
      for (let [index, existingKey] of prev.entries()) {
        if (key.includes("precipitation")) {
          if (value[index] > 80) {
            existingKey = { ...existingKey, weather: "rainy" };
          }
          if (value[index] > 20 && value[index] < 80) {
            existingKey = { ...existingKey, weather: "cloudy" };
          } else {
            existingKey = { ...existingKey, weather: "sunny" };
          }
        }
        existingKey = { ...existingKey, [key]: value[index] };
        prev[index] = existingKey;
      }
      return prev;
    },
    new Array(7)
  );
  return parsed;
};

const dayEightMain = async () => {
  try {
    const weather = await getWeather();
    const updateContent = weatherComponent(weather);
    const [wrapperToUpdate, ...rest] =
      document.getElementsByClassName("wrapper");
    wrapperToUpdate.innerHTML += updateContent;
  } catch (e) {
    throw new Error("Something went wrong overall");
  }
};
export default dayEightMain;
