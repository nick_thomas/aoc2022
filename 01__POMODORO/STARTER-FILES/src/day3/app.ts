const dayThreeMain = () => {
  const getWhiteKeys = getKeys("white-keys");
  const getBlackKeys = getKeys("black-keys");
  const allKeys = [
    ...Object.values(getWhiteKeys),
    ...Object.values(getBlackKeys),
  ];
  attachAudio(allKeys);
};

const attachAudio = (keys: Array<Element>) => {
  const playAudio = (button: Element, index: number) => {
    let audio = new Audio(`./key-${index}.mp3`);
    audio.play();
  };
  Object.values(keys).forEach((key, index) => {
    key.addEventListener("click", playAudio.bind(null, key, index + 1));
  });
};

const getKeys = (className: string) =>
  document.getElementsByClassName(className);

export default dayThreeMain;
