import { head, isNotNil, length, mergeAll, zip } from "ramda";

type Response = { [key: string]: FormDataEntryValue };

const submit = (
  e: Event,
  form: HTMLFormElement,
  value?: string,
  data?: Response
) => {
  if (data === undefined) {
    return;
  }
  e.preventDefault();
  const dataForm = new FormData(form);
  if (isNotNil(value) && length(value) === 4) {
    let pastedValue = value.split("");
    for (let [key, item] of zip([...dataForm.keys()], pastedValue)) {
      data[key] = item;
    }

    // Fix this
    let inputValue = [...form.elements].filter(
      (item) => item.tagName.toLowerCase() === "input"
    );
    for (let [input, item] of zip([...inputValue], pastedValue)) {
      (input as HTMLInputElement).value = item;
    }

    //
  } else {
    for (let [key, dataStore] of dataForm) {
      data[key] = dataStore;
    }
  }
  //  for (const element of form.elements) {
  //    if (element.tagName.toLowerCase() === "input") {
  //      (element as HTMLInputElement).value = "2";
  //    }
  //  }
  return data;
};
const formSetup = (value: string, data: Response) => {
  const form = head([...document.forms]);
  if (form !== undefined) {
    // return the value up the call stack
    form.onsubmit = (e) => submit(e, form, value, data);
  }
};

const pasteListener = (e: Event, submittedData: Response) => {
  e.preventDefault();
  document.onpaste = (e) => {
    e.preventDefault();
    let pastedValue = e?.clipboardData!.getData("text/plain");
    if (pastedValue.length <= 3) {
      return;
    }
    formSetup(pastedValue, submittedData);
    const form = head([...document.forms]);
    if (form !== undefined) {
      // return the value up the call stack
      submit(e, form, pastedValue, submittedData);
    }
  };
};

const day10Main = () => {
  let submittedData: Response = {};
  document.addEventListener("visibilitychange", (e) =>
    pasteListener(e, submittedData)
  );
};
export default day10Main;
