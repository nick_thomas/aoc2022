const addingEventToKeys = (querySelector: string): NodeListOf<Element> => {
  const getKeys = document.querySelectorAll(querySelector);
  return getKeys;
};

const getEventAttachedToKeys = () =>
  addingEventToKeys("button.key:not(.utility)");

const dayFourMain = () => {
  const keys = getEventAttachedToKeys();
  const buttonJiggle = new ButtonJiggler(keys);
  buttonJiggle.initiateJiggle();
};

const getRandom = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min) + min);
};

class ButtonJiggler {
  // Generate jiggle on initial load
  // on press of key, change random key
  keys: NodeListOf<Element>;
  lastActiveKey: Element | null;
  handler: null | ReturnType<typeof setInterval>;
  body: void | null;
  constructor(keys: NodeListOf<Element>) {
    this.keys = keys;
    this.lastActiveKey = null;
    this.handler = null;
    this.body = null;
  }
  genereateRandomJiggle() {
    const randomKey = getRandom(0, this.keys.length);
    if (this.lastActiveKey === null) {
      this.lastActiveKey = this.keys[randomKey];
      this.keys[randomKey].classList.add("jiggle");
      this.lastActiveKey = this.keys[randomKey];
    } else {
      let newRandomKey = getRandom(0, this.keys.length);
      this.lastActiveKey.classList.remove("jiggle");
      this.keys[newRandomKey].classList.add("jiggle");
      this.lastActiveKey = this.keys[newRandomKey];
    }
  }
  initiateJiggle() {
    this.setupGlobalListener();
    this.genereateRandomJiggle();
  }
  setupGlobalListener() {
    this.body = addEventListener("keydown", (event) => {
      if (this.lastActiveKey?.hasAttribute("data-key")) {
        const dataKey = this.lastActiveKey.getAttribute("data-key");
        const inputKey = event.key;
        if (dataKey.toLowerCase() === inputKey.toLowerCase()) {
          // add logic to change jiggle
          this.genereateRandomJiggle();
        }
      }
    });
  }
}

export default dayFourMain;
